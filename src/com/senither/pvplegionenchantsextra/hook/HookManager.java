package com.senither.pvplegionenchantsextra.hook;

import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;

public class HookManager
{

    private final Hook vault;
    private final Hook token;

    public HookManager(PvPLegionEnchantsExtra plugin)
    {
        this.vault = ((plugin.getServer().getPluginManager().isPluginEnabled("Vault")) ? new VaultHook(plugin) : new Hook(plugin));
        this.token = ((plugin.getServer().getPluginManager().isPluginEnabled("MySQL-Tokens")) ? new TokenHook(plugin) : new Hook(plugin));
    }

    public Hook getVault()
    {
        return vault;
    }

    public Hook getToken()
    {
        return token;
    }
}
