package com.senither.pvplegionenchantsextra.hook;

import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;
import org.bukkit.entity.Player;

public class Hook
{

    protected final PvPLegionEnchantsExtra plugin;
    protected boolean enabled;

    public Hook(PvPLegionEnchantsExtra plugin)
    {
        this.plugin = plugin;
        this.enabled = false;
    }

    public Hook(PvPLegionEnchantsExtra plugin, boolean enabled)
    {
        this.plugin = plugin;
        this.enabled = enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public double getCurrency(Player player)
    {
        return -1;
    }

    public boolean takeCurrency(Player player, double currency)
    {
        return true;
    }

    public boolean giveCurrency(Player player, double currency)
    {
        return true;
    }
}
