package com.senither.pvplegionenchantsextra.hook;

import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;
import me.bukkit.mTokens.Inkzzz.Tokens;
import org.bukkit.entity.Player;

public class TokenHook extends Hook
{

    public TokenHook(PvPLegionEnchantsExtra plugin)
    {
        super(plugin, true);
    }

    @Override
    public double getCurrency(Player player)
    {
        return Tokens.getInstance().getAPI().getTokens(player);
    }

    @Override
    public boolean takeCurrency(Player player, double currency)
    {
        Tokens.getInstance().getAPI().takeTokens(player, (int) currency);
        return true;
    }

    @Override
    public boolean giveCurrency(Player player, double currency)
    {
        Tokens.getInstance().getAPI().giveTokens(player, (int) Math.round(currency));
        return true;
    }
}
