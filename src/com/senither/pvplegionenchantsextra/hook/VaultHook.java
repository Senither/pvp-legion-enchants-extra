package com.senither.pvplegionenchantsextra.hook;

import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultHook extends Hook
{

    private Economy economy = null;

    public VaultHook(PvPLegionEnchantsExtra plugin)
    {
        super(plugin);
        setEnabled(setupEconomy());
    }

    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return (economy != null);
    }

    @Override
    public double getCurrency(Player player)
    {
        if (isEnabled()) {
            return economy.getBalance(player);
        }
        return -1;
    }

    @Override
    public boolean takeCurrency(Player player, double currency)
    {
        if (isEnabled()) {
            EconomyResponse er = economy.withdrawPlayer(player, currency);
            return er.transactionSuccess();
        }
        return false;
    }

    @Override
    public boolean giveCurrency(Player player, double currency)
    {
        if (isEnabled()) {
            EconomyResponse er = economy.depositPlayer(player, currency);
            return er.transactionSuccess();
        }
        return false;
    }
}
