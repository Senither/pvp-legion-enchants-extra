package com.senither.pvplegionenchantsextra.utils;

import com.senither.pvplegionenchants.Enchantments;
import org.bukkit.Location;

public class Container
{

    private final String name;
    private final Location location;
    private final Enchantments enchantment;

    public Container(String name, Location location, Enchantments enchantment)
    {
        this.name = name;
        this.location = location;
        this.enchantment = enchantment;
    }

    public String getName()
    {
        return name;
    }

    public Location getLocation()
    {
        return location;
    }

    public Enchantments getEnchantment()
    {
        return enchantment;
    }
}
