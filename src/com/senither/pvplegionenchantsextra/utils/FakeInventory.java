package com.senither.pvplegionenchantsextra.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class FakeInventory
{

    private final String title;
    private final int size;
    private final HashMap<Integer, ItemStack> items;

    public FakeInventory(String title, int rows)
    {
        this.title = colorize(title);

        rows = (rows < 1) ? 1 : rows;
        rows = (rows > 6) ? 6 : rows;

        size = rows * 9;

        items = new HashMap<>();
    }

    public FakeInventory unset(int index)
    {
        items.remove(index);
        return this;
    }

    public FakeInventory unset(int from, int to)
    {
        if (from < to) {
            for (int i = from; i <= to; i++) {
                unset(i);
            }
        } else {
            for (int i = to; i <= from; i++) {
                unset(i);
            }
        }
        return this;
    }

    public FakeInventory createLine(int row, ItemStack item)
    {
        for (int i = row * 9; i < (9 + (row * 9)); i++) {
            items.put(i, item);
        }
        return this;
    }

    public FakeInventory createLine(int row, ItemStack item, String name)
    {
        return createLine(row, buildItem(item, name));
    }

    public FakeInventory createLine(int row, ItemStack item, String name, List<String> lore)
    {
        return createLine(row, buildItem(item, name, lore));
    }

    public FakeInventory createLine(int row, Material material, String name)
    {
        return createLine(row, buildItem(material, name));
    }

    public FakeInventory createLine(int row, Material material, String name, List<String> lore)
    {
        return createLine(row, buildItem(material, name, lore));
    }

    public FakeInventory createWall(WallSide side, ItemStack item)
    {
        int index = -1;
        int multiplier = -1;
        int timers = -1;

        switch (side) {
            case ALL:
                createWall(WallSide.TOP, item);
                createWall(WallSide.RIGHT, item);
                createWall(WallSide.BOTTOM, item);
                createWall(WallSide.LEFT, item);
                break;
            case TOP:
                index = 0;
                multiplier = 1;
                timers = 9;
                break;
            case RIGHT:
                index = 8;
                multiplier = 9;
                timers = 6;
                break;
            case BOTTOM:
                index = size - 8;
                multiplier = 1;
                timers = 9;
                break;
            case LEFT:
                index = 0;
                multiplier = 9;
                timers = 6;
                break;
        }

        if (index == -1 || multiplier == -1 || timers == -1) {
            return this;
        }

        for (int i = 0; i < timers; i++) {
            items.put(index, item);
            index += multiplier;
        }

        return this;
    }

    public FakeInventory createWall(WallSide side, ItemStack item, String name)
    {
        return createWall(side, buildItem(item, name));
    }

    public FakeInventory createWall(WallSide side, ItemStack item, String name, List<String> lore)
    {
        return createWall(side, buildItem(item, name, lore));
    }

    public FakeInventory createWall(WallSide side, Material material, String name)
    {
        return createWall(side, buildItem(material, name));
    }

    public FakeInventory createWall(WallSide side, Material material, String name, List<String> lore)
    {
        return createWall(side, buildItem(material, name, lore));
    }

    public FakeInventory fill(ItemStack item)
    {
        for (int i = 0; i < (size / 9); i++) {
            createLine(i, item);
        }
        return this;
    }

    public FakeInventory fill(ItemStack item, String name)
    {
        return fill(buildItem(item, name));
    }

    public FakeInventory fill(ItemStack item, String name, List<String> lore)
    {
        return fill(buildItem(item, name, lore));
    }

    public FakeInventory setItem(int position, ItemStack item)
    {
        try {
            items.put(position, item);
        } catch (IndexOutOfBoundsException e) {

        }
        return this;
    }

    public FakeInventory addItem(ItemStack item)
    {
        for (int i = 0; i < size; i++) {
            if (!items.containsKey(i)) {
                items.put(i, item);
                break;
            }
        }
        return this;
    }

    public FakeInventory addItem(Material material, String name)
    {
        return addItem(buildItem(material, name));
    }

    public FakeInventory addItem(Material material, String name, List<String> lore)
    {
        return addItem(buildItem(material, name, lore));
    }

    public FakeInventory setItem(int position, ItemStack item, String name)
    {
        return setItem(position, buildItem(item, name));
    }

    public FakeInventory setItem(int position, ItemStack item, String name, List<String> lore)
    {
        return setItem(position, buildItem(item, name, lore));
    }

    public FakeInventory setItem(int position, Material material, String name)
    {
        return setItem(position, buildItem(material, name));
    }

    public FakeInventory setItem(int position, Material material, String name, List<String> lore)
    {
        return setItem(position, buildItem(material, name, lore));
    }

    public Inventory build()
    {
        Inventory inventory = Bukkit.createInventory(null, size, title);

        for (int i = 0; i < size; i++) {
            if (items.containsKey(i) && items.get(i) != null) {
                inventory.setItem(i, items.get(i));
            }
        }

        return inventory;
    }

    /**
     * Opens the inventory for the given player
     *
     * @param player Opens the inventory for the player
     */
    public void open(Player player)
    {
        player.openInventory(build());
    }

    /**
     * Colorizes the string so it's colorful.
     *
     * @param str The string to color
     * @return The colored string
     */
    private String colorize(String str)
    {
        return ChatColor.translateAlternateColorCodes('&', str);
    }

    private ItemStack buildItem(ItemStack item, String name)
    {
        if (name != null) {
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(colorize(name));
            item.setItemMeta(meta);
        }

        return item;
    }

    private ItemStack buildItem(ItemStack item, String name, List<String> lore)
    {
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(colorize(name));

        List<String> placeholder = new ArrayList<>();
        for (String line : lore) {
            placeholder.add(colorize(line));
        }
        meta.setLore(placeholder);

        item.setItemMeta(meta);
        return item;
    }

    private ItemStack buildItem(Material material, String name)
    {
        ItemStack item = new ItemStack(material, 1);

        if (name != null) {
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(colorize(name));
            item.setItemMeta(meta);
        }

        return item;
    }

    private ItemStack buildItem(Material material, String name, List<String> lore)
    {
        ItemStack item = new ItemStack(material, 1);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(colorize(name));

        List<String> placeholder = new ArrayList<>();
        for (String line : lore) {
            placeholder.add(colorize(line));
        }
        meta.setLore(placeholder);

        item.setItemMeta(meta);
        return item;
    }

    public enum WallSide
    {

        ALL("All", 0),
        TOP("Top", 1),
        RIGHT("Right", 2),
        BOTTOM("Bottom", 3),
        LEFT("Left", 4);

        private final String name;
        private final int id;

        private WallSide(String name, int id)
        {
            this.name = name;
            this.id = id;
        }

        public String getName()
        {
            return name;
        }

        public int getId()
        {
            return id;
        }
    }
}
