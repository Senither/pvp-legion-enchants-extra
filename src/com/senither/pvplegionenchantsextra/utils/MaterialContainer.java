package com.senither.pvplegionenchantsextra.utils;

import org.bukkit.Material;

public class MaterialContainer
{

    private Material material;
    private short data;

    public MaterialContainer(Material material, short data)
    {
        this.material = material;
        this.data = data;
    }

    public short getData()
    {
        return data;
    }

    public Material getMaterial()
    {
        return material;
    }

    public void setData(short data)
    {
        this.data = data;
    }

    public void setMaterial(Material material)
    {
        this.material = material;
    }

}
