package com.senither.pvplegionenchantsextra.utils;

import java.util.HashMap;

public class Cost
{

    private final HashMap<Integer, Integer> money = new HashMap<>();
    private final HashMap<Integer, Integer> token = new HashMap<>();

    public Cost setMoneyCost(int level, int cost)
    {
        this.money.put(level, cost);
        return this;
    }

    public Cost setTokenCost(int level, int cost)
    {
        this.token.put(level, cost);
        return this;
    }

    public boolean hasMoneyLevel(int level)
    {
        return this.money.containsKey(level);
    }

    public boolean hasTokenLevel(int level)
    {
        return this.token.containsKey(level);
    }

    public int getMoneyCost(int level)
    {
        return hasMoneyLevel(level) ? this.money.get(level) : -1;
    }

    public int getTokenCost(int level)
    {
        return hasTokenLevel(level) ? this.token.get(level) : -1;
    }
}
