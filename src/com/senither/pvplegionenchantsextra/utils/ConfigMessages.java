package com.senither.pvplegionenchantsextra.utils;

import com.senither.pvplegionenchants.Enchantments;
import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.entity.Player;

public final class ConfigMessages
{

    private final PvPLegionEnchantsExtra plugin;
    private final HashMap<Window, InformationItem> items;
    private String checkoutMessage;
    private static ConfigMessages instance = null;

    public ConfigMessages(PvPLegionEnchantsExtra plugin)
    {
        this.plugin = plugin;
        this.items = new HashMap<>();
        this.checkoutMessage = null;

        this.reloadConfig();
    }

    public void reloadConfig()
    {
        items.clear();

        for (Window window : Window.values()) {
            String name = plugin.getConfig().getString(window.getPath() + ".title");
            List<String> lines = plugin.getConfig().getStringList(window.getPath() + ".lines");

            if (name == null || lines == null) {
                name = "&c[Error Loading Title]";
                lines = new ArrayList<>();
                lines.add("");
                lines.add("&c[Error Loading Lines]");
            }

            items.put(window, new InformationItem(window.getName(), plugin.chat.colorize(name), plugin.chat.colorize(lines)));
        }

        checkoutMessage = plugin.getConfig().getString("checkout-complete-message");
        if (checkoutMessage != null) {
            checkoutMessage = plugin.chat.colorize(checkoutMessage);
        }
    }

    public String getCheckoutMessage()
    {
        return (checkoutMessage == null) ? "&8[&aEnchant&8] &aYou bought &2%enchant% %level% &afor &2%cost% %currency%&a!" : checkoutMessage;
    }

    public String getCheckoutMessage(Player player, Enchantments enchantment, int level, int cost, String currency)
    {
        return getCheckoutMessage()
                .replace("%player%", player.getName())
                .replace("%enchant%", (enchantment == null) ? "" : enchantment.getName())
                .replace("%level%", "" + level)
                .replace("%cost%", "" + cost)
                .replace("%currency%", currency.trim());
    }

    public InformationItem getItem(Window window)
    {
        if (items.containsKey(window)) {
            return items.get(window);
        }
        return null;
    }

    public static void setInstance(ConfigMessages classObj)
    {
        if (classObj != null) {
            instance = classObj;
        }
    }

    public static void reload()
    {
        if (instance != null) {
            instance.reloadConfig();
        }
    }

    public class InformationItem
    {

        private final String string;
        private String title;
        private List<String> lines;

        public InformationItem(String string, String title, List<String> lines)
        {
            this.string = string;
            this.title = title;
            this.lines = lines;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }

        public void setLines(List<String> lines)
        {
            this.lines = lines;
        }

        public String getString()
        {
            return string;
        }

        public String getTitle()
        {
            return title;
        }

        public List<String> getLines()
        {
            return lines;
        }
    }

    public enum Window
    {

        SELECTION("Selection", "information-items.selection-window", 0),
        ENCHANTING("Enchanting", "information-items.enchanting-window", 1),
        CHECKOUT("Checkout", "information-items.checkout-window", 2),
        RETURN("Return", "return-to-selection-item", 3);

        private final String name;
        private final String path;
        private final int id;

        private Window(String name, String path, int id)
        {
            this.name = name;
            this.path = path;
            this.id = id;
        }

        public String getName()
        {
            return name;
        }

        public String getPath()
        {
            return path;
        }

        public int getId()
        {
            return id;
        }

        public Window getWindowByName(String name)
        {
            for (Window wind : Window.values()) {
                if (wind.getName().equalsIgnoreCase(name)) {
                    return wind;
                }
            }
            return null;
        }

        public Window getWindowById(int id)
        {
            for (Window wind : Window.values()) {
                if (wind.getId() == id) {
                    return wind;
                }
            }
            return null;
        }
    }
}
