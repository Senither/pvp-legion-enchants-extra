package com.senither.pvplegionenchantsextra.utils;

public class Permissions
{

    private static final String MAIN = "legionenchants.";

    public static final String GUI = MAIN + "gui";
    public static final String RELOAD = MAIN + "reload";
    public static final String PLACEHOLDER = MAIN + "placeholder";
}
