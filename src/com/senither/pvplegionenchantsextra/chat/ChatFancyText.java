package com.senither.pvplegionenchantsextra.chat;

import com.senither.pvplegionenchants.PvPLegionEnchants;
import com.senither.pvplegionenchants.utils.HookManager;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ChatFancyText
{

    /**
     * Formates a normal message into a json raw message, this should only be used for raw/fancy messages, if not just use the sendMessage method from the ChatManager class.
     *
     * @param message
     * @return the json message
     */
    public String getJsonMessage(String message)
    {
        return String.format(ChatFancyTextFormat.TEXT, message + endString());
    }

    /**
     * Sends a hover message to a player
     *
     * @param player object
     * @param message
     * @param hover
     */
    public void sendHoverMessage(Player player, String message, String hover)
    {
        sendRawMessage(player.getName(), getHoverMessage(message, hover));
    }

    /**
     * Generates the json hover message
     *
     * @param message
     * @param hover
     * @return the json message
     */
    public String getHoverMessage(String message, String hover)
    {
        return String.format(ChatFancyTextFormat.HOVER, message + endString(), hover + endString());
    }

    /**
     * Sends a item message to a player
     *
     * @param player
     * @param message
     * @param item
     */
    public void sendItemMessage(Player player, String message, ItemStack item)
    {
        sendRawMessage(player.getName(), getItemMessage(message, item));
    }

    /**
     * Generates the json item message
     *
     * @param message
     * @param item
     * @return the json message
     */
    public String getItemMessage(String message, ItemStack item)
    {
        if (item == null) {
            throw new IllegalArgumentException("Item can not be null!");
        }

        // Start JSON
        String json = "{";

        // Item build info
        json += "id:" + item.getTypeId() + "s,";
        json += "damage:" + item.getDurability() + "s,";
        json += "count:" + item.getAmount() + "b,";

        // Item Meta check
        if (item.hasItemMeta()) {
            ItemMeta meta = item.getItemMeta();

            // Start Tag
            json += "tag:{";

            // Custom display name and lore check
            if (meta.hasDisplayName() || meta.hasLore()) {
                // Start Display
                json += "display:{";

                if (meta.hasDisplayName()) {
                    json += "Name:\\\"" + meta.getDisplayName() + endString() + "\\\",";
                } else {
                    String name = HookManager.getItemName(item);
                    if (name != null) {
                        json += "Name:\\\"" + name + endString() + "\\\",";
                    }
                }

                if (meta.hasLore()) {
                    json += "Lore:[";
                    for (String line : meta.getLore()) {
                        json += "\\\"" + line + endString() + "\\\",";
                    }
                    json = json.substring(0, (json.length() - 1)) + "],";
                }

                // Format and end Display
                json = json.substring(0, (json.length() - 1)) + "},";
            } else {
                String name = HookManager.getItemName(item);
                if (name != null) {
                    json += "display:{Name:\\\"" + name + endString() + "\\\"},";
                }
            }

            // Check Enchantments
            if (meta.hasEnchants()) {
                // Start Enchants
                json += "ench:[";
                for (Enchantment ench : meta.getEnchants().keySet()) {
                    json += "{id:" + ench.getId() + ",lvl:" + meta.getEnchantLevel(ench) + "},";
                }
                // End Enchants
                json = json.substring(0, (json.length() - 1)) + "]";
            }

            if (json.endsWith(",")) {
                json = json.substring(0, (json.length() - 1));
            }

            // End Tag
            json += "}";
        }

        // Return formated string and end JSON
        return String.format(ChatFancyTextFormat.ITEM, message + endString(), (json + "}"));
    }

    /**
     * Sends a url message to a player
     *
     * @param player
     * @param message
     * @param url
     */
    public void sendUrlMessage(Player player, String message, String url)
    {
        sendRawMessage(player.getName(), getUrlMessage(message, url));
    }

    /**
     * Generates the json item message
     *
     * @param message
     * @param url
     * @return json message
     */
    public String getUrlMessage(String message, String url)
    {
        return String.format(ChatFancyTextFormat.CLICK_URL, message, url);
    }

    /**
     * Sends a url message to a player
     *
     * @param player
     * @param message
     * @param suggestion
     */
    public void sendSuggestionMessage(Player player, String message, String suggestion)
    {
        sendRawMessage(player.getName(), getUrlMessage(message, suggestion));
    }

    /**
     * Generates the json item message
     *
     * @param message
     * @param suggestion
     * @return json message
     */
    public String getSuggestionMessage(String message, String suggestion)
    {
        return String.format(ChatFancyTextFormat.CLICK_SUGGEST, message, suggestion);
    }

    /**
     * Sends a url message to a player
     *
     * @param player
     * @param message
     * @param command
     */
    public void sendRunCommandMessage(Player player, String message, String command)
    {
        sendRawMessage(player.getName(), getUrlMessage(message, command));
    }

    /**
     * Generates the json item message
     *
     * @param message
     * @param command
     * @return json message
     */
    public String getRunCommandMessage(String message, String command)
    {
        return String.format(ChatFancyTextFormat.CLICK_RUN_COMMAND, message, command);
    }
    
    /**
     * Sends a tellraw json message
     *
     * @param player name
     * @param json message
     */
    public void sendRawMessage(String player, String json)
    {
        PvPLegionEnchants.getInstance().getServer().dispatchCommand(PvPLegionEnchants.getInstance().getServer().getConsoleSender(), "tellraw " + player + " " + json.replace("&", "§"));
        // Uncomment for debugging
        //SenEnchants.getInstance().getChatManager().getLogger().log(Level.INFO, "JSON : {0}", json);
    }

    /**
     * Sends a tellraw json message
     *
     * @param player name
     * @param json messages
     */
    public void sendRawMessage(String player, String... json)
    {
        String arr = "{\"text\": \"\",\"extra\": [";
        for (String str : json) {
            if (str == null) {
                continue;
            }
            arr += str + ", ";
        }

        sendRawMessage(player, arr.substring(0, arr.length() - 2) + "]}");
    }

    /**
     * @return the end json formater
     */
    private String endString()
    {
        return "§f§r";
    }
}
