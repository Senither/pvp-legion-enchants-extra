package com.senither.pvplegionenchantsextra.chat;

public class ChatFancyTextFormat
{

    public static final String TEXT = "{\"text\": \"%s\"}";
    public static final String HOVER = "{\"text\":\"\",\"extra\":[{\"text\":\"%s\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"%s\"}}]}";
    public static final String ITEM = "{\"text\":\"\",\"extra\":[{\"text\":\"%s\",\"hoverEvent\":{\"action\":\"show_item\",\"value\":\"%s\"}}]}";
    public static final String CLICK_URL = "{text:\"%s\",clickEvent:{action:open_url,value:\"%s\"}}";
    public static final String CLICK_SUGGEST = "{text:\"%s\",clickEvent:{action:suggest_command,value:\"%s\"}}";
    public static final String CLICK_RUN_COMMAND = "{text:\"%s\",clickEvent:{action:run_command,value:\"%s\"}}";
}
