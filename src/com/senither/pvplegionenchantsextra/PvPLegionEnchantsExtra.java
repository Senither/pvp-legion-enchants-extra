package com.senither.pvplegionenchantsextra;

import com.senither.pvplegionenchants.Enchantments;
import com.senither.pvplegionenchantsextra.utils.Container;
import com.senither.pvplegionenchants.PvPLegionEnchants;
import com.senither.pvplegionenchantsextra.chat.ChatManager;
import com.senither.pvplegionenchantsextra.commands.ExtraCommand;
import com.senither.pvplegionenchantsextra.commands.GUICommand;
import com.senither.pvplegionenchantsextra.commands.PlaceholderCommand;
import com.senither.pvplegionenchantsextra.hook.HookManager;
import com.senither.pvplegionenchantsextra.utils.Configuration;
import com.senither.pvplegionenchantsextra.utils.Cost;
import com.senither.pvplegionenchantsextra.utils.MaterialContainer;
import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

public class PvPLegionEnchantsExtra extends JavaPlugin implements Runnable
{

    public static boolean debug = false;
    public static final int configVersion = 1;
    private static PvPLegionEnchantsExtra instance;

    public Configuration placeholders;
    public Configuration enchantments;
    public PvPLegionEnchants api;
    public ChatManager chat;
    public HookManager hook;
    public final HashMap<Enchantments, MaterialContainer> materials = new HashMap<>();
    public final HashMap<Enchantments, Cost> cost = new HashMap<>();

    private int counter = 0;
    private int update = 0;
    private final HashMap<String, Container> containers = new HashMap<>();

    @Override
    public void onEnable()
    {
        instance = this;
        chat = new ChatManager(this);

        if (!getServer().getPluginManager().isPluginEnabled("PvPLegionEnchant")) {
            chat.getLogger().info("Unable to enable PvP Legion Enchants Extra, missing ");
            chat.getLogger().info("dependency \"PvPLegionEnchant\" to work!");
            chat.getLogger().info("Please install the plugin for this to work.");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        chat.getLogger().info("Connecting to the PvP Legion Enchants API..");
        api = (PvPLegionEnchants) getServer().getPluginManager().getPlugin("PvPLegionEnchant");
        if (api == null) {
            chat.getLogger().info("PvPLegionEnchants was not loaded corretly!");
            chat.getLogger().info("Disabling Extra addon..");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        chat.getLogger().info("API was connected to successfully");

        chat.getLogger().info("Hooking into economy APIs..");
        hook = new HookManager(this);
        chat.getLogger().log(Level.INFO, " - Vault {0}", ((hook.getVault().isEnabled()) ? "was hooked into successfully." : "failed to be hooked into!"));
        chat.getLogger().log(Level.INFO, " - Tokens {0}", ((hook.getToken().isEnabled()) ? "was hooked into successfully." : "failed to be hooked into!"));

        saveDefaultConfig();
        if (getConfig().getInt("version", -1) != configVersion) {
            chat.getLogger().info("[Error] Config version mismatch!");
            chat.getLogger().info("[Error] Resetting config back to default.");
            File tempConfig = new File(getDataFolder(), "config.yml");
            if (tempConfig.exists()) {
                tempConfig.delete();
            }
            saveDefaultConfig();
            reloadConfig();
        }

        debug = getConfig().getBoolean("debug-messages-enabled", false);
        chat.getLogger().log(Level.INFO, "Debugging has been set to \"{0}\"", ((debug) ? "Enabled" : "Disabled"));

        update = getConfig().getInt("update-time", 5);
        if (update < 1) {
            update = 1;
        }
        chat.getLogger().log(Level.INFO, "Starting update timer, executing every {0} ticks.", update);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, this, 20, 1);

        placeholders = new Configuration(this, "placeholders.yml");
        placeholders.saveDefaultConfig();
        chat.getLogger().info("Loading placeholders..");

        if (placeholders.getConfig().contains("placeholders")) {
            for (int i = 0; i < placeholders.getConfig().getInt("placeholders"); i++) {
                String name = placeholders.getConfig().getString("placeholder." + i + ".name", null);
                String location = placeholders.getConfig().getString("placeholder." + i + ".location", null);
                String enchantment = placeholders.getConfig().getString("placeholder." + i + ".enchantment", null);

                if (name == null || location == null || enchantment == null) {
                    continue;
                }

                String[] loc = location.split(",");
                Location locObj = new Location(getServer().getWorld(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]));

                Enchantments enc = Enchantments.getEnchantmentByName(enchantment);
                if (enc == null) {
                    continue;
                }

                containers.put(name, new Container(name, locObj, enc));
            }
        } else {
            placeholders.saveDefaultConfig();
        }
        chat.getLogger().log(Level.INFO, " - {0} placeholders were loaded into memory.", containers.size());

        chat.getLogger().info("Loading enchantments information..");
        enchantments = new Configuration(this, "enchantments.yml");
        enchantments.saveDefaultConfig();

        for (Enchantments enchantment : Enchantments.values()) {
            String name = enchantment.getName().replace(" ", "_").toLowerCase();
            Cost costContainer = new Cost();

            if (!enchantments.getConfig().contains(name)) {
                enchantments.getConfig().set(name + ".material", Material.STONE.toString() + ":0");
                for (int i = 1; i <= enchantment.getLevel(); i++) {
                    enchantments.getConfig().set(name + ".cost.upgrade-" + i + ".money", 9001);
                    enchantments.getConfig().set(name + ".cost.upgrade-" + i + ".token", 9001);
                    costContainer.setMoneyCost(i, -1).setTokenCost(i, -1);
                }
                materials.put(enchantment, new MaterialContainer(Material.STONE, (short) 0));
            } else {
                String mat = enchantments.getConfig().getString(name + ".material", "l:l");
                String[] d = mat.split(":");
                Material material = Material.getMaterial(d[0]);
                if (material != null && material != Material.AIR) {
                    materials.put(enchantment, new MaterialContainer(material, Short.parseShort(d[1])));
                } else {
                    if (debug) {
                        chat.getLogger().log(Level.INFO, "Invalid material given for enchantment \"{0}\"! Resetting back to stone.", enchantment.getName());
                        enchantments.getConfig().set(name + ".material", Material.STONE.toString() + ":0");
                    }
                    materials.put(enchantment, new MaterialContainer(Material.STONE, (short) 0));
                }

                for (int i = 1; i <= enchantment.getLevel(); i++) {
                    int money = enchantments.getConfig().getInt(name + ".cost.upgrade-" + i + ".money", -1);
                    int token = enchantments.getConfig().getInt(name + ".cost.upgrade-" + i + ".token", -1);
                    costContainer.setMoneyCost(i, money).setTokenCost(i, token);
                }
            }
            cost.put(enchantment, costContainer);
        }
        enchantments.saveConfig();
        chat.getLogger().log(Level.INFO, " - {0} enchantments has been loaded into memory.", cost.size());

        chat.getLogger().info("Registering commands..");
        getCommand("enchantmentgui").setExecutor(new GUICommand(this));
        getCommand("legionenchantextra").setExecutor(new ExtraCommand(this));
        getCommand("enchantmentplaceholders").setExecutor(new PlaceholderCommand(this));

        chat.getLogger().info("PvP Legion Enchants Extra has been enabled.");
    }

    @Override
    public void onDisable()
    {
        getServer().getScheduler().cancelTasks(this);

        placeholders.getConfig().set("placeholders", containers.size());

        int i = 0;

        for (Container container : containers.values()) {
            Location l = container.getLocation();

            placeholders.getConfig().set("placeholder." + i + ".name", container.getName());
            placeholders.getConfig().set("placeholder." + i + ".location", l.getWorld().getName() + "," + l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ());
            placeholders.getConfig().set("placeholder." + i + ".enchantment", container.getEnchantment().getName());

            i++;
        }

        placeholders.saveConfig();
    }

    @Override
    public void run()
    {
        if (counter++ < update) {
            return;
        }
        counter = 1;

        for (Container c : containers.values()) {
            api.playEffect(c.getEnchantment(), c.getLocation());
        }
    }

    public void setUpdate(int update)
    {
        this.update = update;
    }

    public HashMap<String, Container> getContainers()
    {
        return containers;
    }

    public static PvPLegionEnchantsExtra getInstance()
    {
        return instance;
    }
}
