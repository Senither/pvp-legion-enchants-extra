package com.senither.pvplegionenchantsextra.commands;

import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;
import com.senither.pvplegionenchantsextra.chat.ChatFancyText;
import com.senither.pvplegionenchantsextra.chat.ChatManager;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class Command implements CommandExecutor
{

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args)
    {
        if (requireSenderIsPlayer()) {
            if (sender instanceof Player) {
                return runCommand(sender, label, args);
            }

            getChat().getLogger().info("You just attempted to use a command under PvPLegionEnchantsExtra that requires you to be a player,");
            getChat().getLogger().info("please login to game on an account with with sufficient permissions, and run the command again.");
            return false;
        }

        return runCommand(sender, label, args);
    }

    public ChatManager getChat()
    {
        return PvPLegionEnchantsExtra.getInstance().chat;
    }

    public ChatFancyText getFancyChat()
    {
        return getChat().getFancyText();
    }

    protected void invalidCommand(CommandSender sender, String command)
    {
        getChat().sendMessage(sender, "&7[&c-&7]&m--&7[ &cInvalid command &4" + command);
    }

    public abstract boolean runCommand(CommandSender player, String label, String[] args);

    public abstract boolean requireSenderIsPlayer();
}
