package com.senither.pvplegionenchantsextra.commands;

import com.senither.pvplegionenchants.Enchantments;
import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;
import com.senither.pvplegionenchantsextra.chat.ChatFancyText;
import com.senither.pvplegionenchantsextra.utils.Container;
import com.senither.pvplegionenchantsextra.utils.Permissions;
import java.util.Arrays;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlaceholderCommand extends Command
{

    private final PvPLegionEnchantsExtra plugin;

    public PlaceholderCommand(PvPLegionEnchantsExtra plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public boolean requireSenderIsPlayer()
    {
        return true;
    }

    @Override
    public boolean runCommand(CommandSender player, String label, String[] args)
    {
        if (!player.hasPermission(Permissions.PLACEHOLDER)) {
            getChat().missingPermission(player, Permissions.PLACEHOLDER);
            return false;
        }

        ChatFancyText msg = getFancyChat();

        // Help command
        if (args.length == 0 || args[0].equalsIgnoreCase("help")) {
            msg.sendRawMessage(player.getName(), msg.getJsonMessage("&7[&6+&7]&m--&7[ &6&lLegionEnchant Placeholder Help Menu &7]&7&m--&7[&6+&7]"));

            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchantP &8<&blist&8>", "/lenchantp list "),
                    msg.getJsonMessage("&f: List placeholders")
            );

            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchantP &8<&aadd&8> &8[&aname&8] &8[&aenchantment&8]", "/lenchantp add name enchantment "),
                    msg.getJsonMessage("&f: Add placeholder")
            );

            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchantP &8<&cremove&8> &8[&cname&8]", "/lenchantp remove name"),
                    msg.getJsonMessage("&f: Remove placeholder")
            );
        } else if (args[0].equalsIgnoreCase("list")) {
            msg.sendRawMessage(player.getName(), msg.getJsonMessage("&7[&b+&7]&m--&7[ &b&lLegionEnchants Placeholder List &7]&7&m--&7[&b+&7]"));

            int count = 0;
            String[] json = new String[plugin.getContainers().size() * 2];
            for (Container container : plugin.getContainers().values()) {
                ItemStack item = new ItemStack(Material.STONE);
                ItemMeta meta = item.getItemMeta();

                Location l = container.getLocation();

                meta.setDisplayName(getChat().colorize("&7" + container.getName()));
                meta.setLore(Arrays.asList(
                        "",
                        "&bEnchantment&7: " + container.getEnchantment().getName(),
                        "&bLocation&7: " + l.getWorld().getName() + " &8@&7 X:" + l.getBlockX() + "&8,&7 Y:" + l.getBlockY() + "&8,&7 Z:" + l.getBlockZ(),
                        ""
                ));
                item.setItemMeta(meta);

                json[count++] = msg.getItemMessage("&7" + container.getName(), item);
                json[count++] = msg.getJsonMessage("&8, ");
            }

            if (json.length == 0) {
                getChat().sendMessage(player, "&7There are currently no placeholders..");
            } else {
                json[json.length - 1] = null;
                msg.sendRawMessage(player.getName(), json);
            }
            return true;
        } else if (args[0].equalsIgnoreCase("add")) {
            if (args.length < 3) {
                getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cMissing paramaters, you need three(3) arguments");
                getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cfor the add command.");
                return false;
            }

            String name = args[1];

            if (plugin.getContainers().containsKey(name)) {
                getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cPlaceholder \"" + name + "\" already exists.");
                return false;
            }

            Enchantments enchantment = Enchantments.getEnchantmentByName(args[2].replaceAll("_", " "));

            if (enchantment == null) {
                getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cThe given enchantment is not valid!");
                getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cYou can use &4/lenc list &cto see a list of the enchants");
                return false;
            }

            Location l = ((Player) player).getLocation();

            plugin.getContainers().put(name, new Container(name, new Location(l.getWorld(), l.getBlockX(), l.getBlockY(), l.getBlockZ()), enchantment));
            getChat().sendMessage(player, "&7[&e+&7]&m--&7[ &aPlaceholder &2" + name + "&a has been set to your location.");
        } else if (args[0].equalsIgnoreCase("remove")) {
            if (args.length < 2) {
                getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cMissing paramaters, you need two(2) arguments ");
                getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cfor the remove command.");
                return false;
            }

            String name = args[1];

            if (!plugin.getContainers().containsKey(name)) {
                getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cPlaceholder &2" + name + "&c doesn't exists.");
                return false;
            }

            plugin.getContainers().remove(name);
            getChat().sendMessage(player, "&7[&e+&7]&m--&7[ &aPlaceholder &2" + name + "&a has been removed.");
        } else {
            String command = "/LEncP ";
            for (String arg : args) {
                command += arg + " ";
            }
            invalidCommand(player, command.trim());
        }
        return true;
    }
}
