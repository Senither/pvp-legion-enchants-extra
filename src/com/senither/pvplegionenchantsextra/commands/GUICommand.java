package com.senither.pvplegionenchantsextra.commands;

import com.senither.pvplegionenchants.EnchantmentType;
import com.senither.pvplegionenchants.Enchantments;
import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;
import com.senither.pvplegionenchantsextra.utils.ConfigMessages;
import com.senither.pvplegionenchantsextra.utils.Cost;
import com.senither.pvplegionenchantsextra.utils.FakeInventory;
import com.senither.pvplegionenchantsextra.utils.MaterialContainer;
import com.senither.pvplegionenchantsextra.utils.Permissions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GUICommand extends Command implements Listener
{

    private final PvPLegionEnchantsExtra plugin;
    private final String listTitle;
    private final String enchantmentTitle;
    private final String checkoutTitle;

    private final HashMap<String, Map<String, Object>> itemsCache;
    private final ConfigMessages messages;

    public GUICommand(PvPLegionEnchantsExtra plugin)
    {
        this.plugin = plugin;
        this.plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.itemsCache = new HashMap<>();
        this.messages = new ConfigMessages(plugin);
        ConfigMessages.setInstance(messages);

        // List inventory properties
        this.listTitle = getChat().colorize("&8Select item to enchant");
        this.enchantmentTitle = getChat().colorize("&8Enchantment Window");
        this.checkoutTitle = getChat().colorize("&8Checkout Window");
    }

    @Override
    public boolean requireSenderIsPlayer()
    {
        return false;
    }

    @Override
    public boolean runCommand(CommandSender player, String label, String[] args)
    {
        if (!player.hasPermission(Permissions.GUI)) {
            getChat().missingPermission(player, Permissions.GUI);
            return false;
        }

        if (args.length == 0) {
            getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cYou have to include a player name");
            getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &cthat the GUI should open for!");
            getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &4/LEnchantGUI &c<&4name&c>");
            return false;
        }

        Player target = plugin.getServer().getPlayer(args[0]);

        if (target == null || !target.isOnline()) {
            getChat().sendMessage(player, "&7[&c-&7]&m--&7[ &4" + args[0] + " &cis not online!");
            return false;
        }

        showListWindow(target);
        return true;
    }

    private void showListWindow(Player player)
    {
        FakeInventory inventory = new FakeInventory(listTitle, 6);
        inventory.createWall(FakeInventory.WallSide.ALL, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");

        ConfigMessages.InformationItem infoItem = messages.getItem(ConfigMessages.Window.SELECTION);
        inventory.setItem(position(5, 4), Material.NETHER_STAR, infoItem.getTitle(), infoItem.getLines());

        for (ItemStack item : player.getInventory().getContents()) {
            if (item != null) {
                if (EnchantmentType.getType(item.getType()) != null) {
                    inventory.addItem(item);
                }
            }
        }

        inventory.open(player);
    }

    private void showEnchantmentWindow(Player player, ItemStack item, Enchantments enchantment)
    {
        itemsCache.put(player.getName(), item.serialize());

        FakeInventory inventory = new FakeInventory(enchantmentTitle, 6);
        // Fillers
        inventory.createLine(2, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15), "&7Filler Pane");
        inventory.createLine(3, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15), "&7Filler Pane");
        inventory.createLine(4, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15), "&7Filler Pane");
        // Create wall & overwrite other fillers
        inventory.createWall(FakeInventory.WallSide.ALL, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.createLine(1, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.unset(1, 7);

        ConfigMessages.InformationItem returnItem = messages.getItem(ConfigMessages.Window.RETURN);
        inventory.setItem(48, Material.ARROW, returnItem.getTitle(), returnItem.getLines());

        ConfigMessages.InformationItem infoItem = messages.getItem(ConfigMessages.Window.ENCHANTING);
        inventory.setItem(50, Material.NETHER_STAR, infoItem.getTitle(), infoItem.getLines());

        EnchantmentType type = EnchantmentType.getType(item.getType());

        for (Enchantments enc : Enchantments.values()) {
            if (enchantesWindowsTypeCheck(type, enc.getClassName().getType())) {
                MaterialContainer mc = plugin.materials.get(enc);
                ItemStack placeholder = new ItemStack(mc.getMaterial(), 1, mc.getData());
                ItemMeta meta = placeholder.getItemMeta();

                List<String> info = new ArrayList<>(enc.getClassName().getEnchantmentInformation());

                if (info.isEmpty()) {
                    meta.setDisplayName(getChat().colorize(enc.getName()));
                    meta.setLore(getChat().colorize(Arrays.asList("&7This enchantment have not been setup yet!", "&7Please create it in the config.")));
                } else {
                    meta.setDisplayName(getChat().colorize(info.get(0)));
                    info.remove(0);

                    List<String> modifiers = new ArrayList<>();
                    modifiers.add("");

                    for (String line : info) {
                        modifiers.add(line);
                    }

                    meta.setLore(getChat().colorize(modifiers));
                }
                placeholder.setItemMeta(meta);
                inventory.addItem(placeholder);
            }
        }

        int costMoney = -1;
        int costToken = -1;

        ItemStack newItem = new ItemStack(item);
        int level = plugin.api.getEnchantmentLevel(enchantment, newItem) + 1;

        if (level < 1) {
            level = 1;
        }

        // Fillers
        // The active item
        inventory.setItem(position(3, 2), item);

        // Red Pane
        inventory.setItem(position(3, 3), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), "&7Filler Pane");

        // Enchantment Icon
        if (enchantment != null) {
            if (level <= enchantment.getLevel()) {
                MaterialContainer mc = plugin.materials.get(enchantment);
                ItemStack enchant = new ItemStack(mc.getMaterial(), 1, mc.getData());
                ItemMeta meta = enchant.getItemMeta();
                List<String> info = new ArrayList<>(enchantment.getClassName().getEnchantmentInformation());

                if (info.isEmpty()) {
                    meta.setDisplayName(getChat().colorize(enchantment.getName()));
                    meta.setLore(getChat().colorize(Arrays.asList("&7This enchantment have not been setup yet!", "&7Please create it in the config.")));
                } else {
                    meta.setDisplayName(getChat().colorize(info.get(0)));
                    info.remove(0);

                    List<String> modifiers = new ArrayList<>();
                    modifiers.add("");

                    for (String line : info) {
                        modifiers.add(line);
                    }

                    meta.setLore(getChat().colorize(modifiers));
                }
                enchant.setItemMeta(meta);
                inventory.setItem(position(3, 4), enchant);
            } else {
                inventory.setItem(position(3, 4), new ItemStack(Material.REDSTONE_TORCH_ON, 1), "&cNotice", Arrays.asList(
                        "",
                        "&7You already have that enchantment",
                        "&7at max level on your item silly.",
                        ""
                ));
            }
        } else {
            inventory.setItem(position(3, 4), new ItemStack(Material.AIR, 1), null);
        }

        // Green Pane
        inventory.setItem(position(3, 5), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5), "&7Filler Pane");

        if (enchantment != null) {
            if (level <= enchantment.getLevel()) {
                Cost c = plugin.cost.get(enchantment);

                costMoney = c.getMoneyCost(level);
                costToken = c.getTokenCost(level);

                newItem = plugin.api.enchantItem(newItem, enchantment, level);
                inventory.setItem(position(3, 6), newItem);
            } else {
                inventory.setItem(position(3, 6), new ItemStack(Material.AIR, 1), null);
            }
        } else {
            inventory.setItem(position(3, 6), new ItemStack(Material.AIR, 1), null);
        }

        // Generate the cost
        List<String> costLore;

        if (costMoney != -1 && costToken != -1) {
            costLore = Arrays.asList(
                    "",
                    "&6Cost in $: &e" + costMoney,
                    "&6Cost in Tokens: &e" + costToken,
                    ""
            );
        } else {
            costLore = Arrays.asList(
                    "",
                    "&7When you have selected a",
                    "&7enchantment, the cost of it will",
                    "&7be displayed here.",
                    ""
            );
        }

        inventory.setItem(49, Material.DIAMOND, "&6Enchantment Cost", costLore);

        inventory.open(player);
    }

    private void showCheckoutWindow(Player player, ItemStack item, Enchantments enchantment)
    {
        FakeInventory inventory = new FakeInventory(checkoutTitle, 6);
        // Fillers
        inventory.fill(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15), "&7Filler Pane");
        // Diamond
        inventory.setItem(position(2, 1), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.setItem(position(2, 3), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.setItem(position(1, 2), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.setItem(position(3, 2), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        // Elerald
        inventory.setItem(position(2, 5), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.setItem(position(2, 7), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.setItem(position(1, 6), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.setItem(position(3, 6), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        // Item
        inventory.setItem(position(0, 3), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.setItem(position(0, 5), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");
        inventory.setItem(position(1, 4), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&7Filler Pane");

        ConfigMessages.InformationItem returnItem = messages.getItem(ConfigMessages.Window.RETURN);
        inventory.setItem(48, Material.ARROW, returnItem.getTitle(), returnItem.getLines());

        ConfigMessages.InformationItem infoItem = messages.getItem(ConfigMessages.Window.CHECKOUT);
        inventory.setItem(50, Material.NETHER_STAR, infoItem.getTitle(), infoItem.getLines());

        Cost cost = plugin.cost.get(enchantment);
        int level = plugin.api.getEnchantmentLevel(enchantment, item);
        String disabled = getChat().colorize(" &8&l(&7Disabled&8&l)");

        if (plugin.hook.getVault().isEnabled()) {
            if (plugin.hook.getVault().getCurrency(player) >= cost.getMoneyCost(level)) {
                inventory.setItem(position(2, 2), Material.DIAMOND, "&bPay $" + cost.getMoneyCost(level), Arrays.asList(
                        "",
                        "&7Upon clicking on this button you will",
                        "&7be charged &b$" + cost.getMoneyCost(level) + " &7for the enchant:",
                        "",
                        "&7" + enchantment.getName() + " " + intToEnchantmentLevel(level),
                        "")
                );
            } else {
                inventory.setItem(position(2, 2), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), "&cYou have insufficient funds!", Arrays.asList(
                        "",
                        "&7You have &b$" + ((int) plugin.hook.getVault().getCurrency(player)) + "&7.",
                        "&7The enchantment costs &c$" + cost.getMoneyCost(level) + "&7!",
                        ""
                ));
            }
        } else {
            inventory.setItem(position(2, 2), Material.DIAMOND, "&bPay $" + cost.getMoneyCost(level) + disabled, Arrays.asList(
                    "",
                    "&7This feature is currently &cdisabled&7!",
                    ""
            ));
        }

        if (plugin.hook.getToken().isEnabled()) {
            if (plugin.hook.getToken().getCurrency(player) >= cost.getTokenCost(level)) {
                inventory.setItem(position(2, 6), Material.EMERALD, "&6Pay " + cost.getTokenCost(level) + " Tokens", Arrays.asList(
                        "",
                        "&7Upon clicking on this button you will",
                        "&7be charged &6" + cost.getTokenCost(level) + " &7tokens for the enchant:",
                        "",
                        "&7" + enchantment.getName() + " " + intToEnchantmentLevel(level),
                        "")
                );
            } else {
                inventory.setItem(position(2, 6), new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), "&cYou have insufficient funds!", Arrays.asList(
                        "",
                        "&7You have &6" + ((int) plugin.hook.getToken().getCurrency(player)) + " tokens&7.",
                        "&7The enchantment costs &c" + cost.getTokenCost(level) + " tokens&7!",
                        ""
                ));
            }
        } else {
            inventory.setItem(position(2, 6), Material.EMERALD, "&6Pay " + cost.getTokenCost(level) + " Tokens" + disabled, Arrays.asList(
                    "",
                    "&7This feature is currently &cdisabled&7!",
                    ""
            ));
        }

        inventory.setItem(position(0, 4), item);

        inventory.open(player);
    }

    private boolean enchantesWindowsTypeCheck(EnchantmentType type, EnchantmentType[] types)
    {
        for (EnchantmentType t : types) {
            if (t.getChild() != null) {
                for (EnchantmentType e : t.getChild()) {
                    if (e.equals(type)) {
                        return true;
                    }
                }
                continue;
            }
            if (t.equals(type)) {
                return true;
            }
        }
        return false;
    }

    private int position(int row, int index)
    {
        return (row * 9) + index;
    }

    private String intToEnchantmentLevel(int level)
    {
        if (level < 1) {
            level = 1;
        }

        switch (level) {
            case 1:
                return "I";
            case 2:
                return "II";
            case 3:
                return "III";
            case 4:
                return "IV";
            case 5:
                return "V";
            case 6:
                return "VI";
            case 7:
                return "VII";
            case 8:
                return "VIII";
            case 9:
                return "IX";
            case 10:
                return "X";
            default:
                return "I";
        }
    }

    private int stringToEnchantmentLevel(String level)
    {
        switch (level) {
            case "I":
                return 1;
            case "II":
                return 2;
            case "III":
                return 3;
            case "IV":
                return 4;
            case "V":
                return 5;
            case "VI":
                return 6;
            case "VII":
                return 7;
            case "VIII":
                return 8;
            case "IX":
                return 9;
            case "X":
                return 10;
            default:
                return 1;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInventoryClick(InventoryClickEvent e)
    {
        if (e.getInventory().getTitle().equalsIgnoreCase(listTitle)) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null) {
                if (EnchantmentType.getType(e.getCurrentItem().getType()) != null) {
                    showEnchantmentWindow((Player) e.getWhoClicked(), e.getCurrentItem(), null);
                }
            }
        } else if (e.getInventory().getTitle().equalsIgnoreCase(enchantmentTitle)) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null) {
                ItemStack item = e.getCurrentItem();

                if (item.getType().equals(Material.ARROW) && item.hasItemMeta()) {
                    ItemMeta meta = item.getItemMeta();
                    if (meta.hasDisplayName()) {
                        showListWindow((Player) e.getWhoClicked());
                        return;
                    }
                }

                int slot = e.getRawSlot();
                if (slot >= 1 && slot <= 7) {
                    if (item.hasItemMeta()) {
                        ItemMeta meta = item.getItemMeta();
                        if (meta.hasDisplayName()) {
                            Enchantments enchantment = Enchantments.getEnchantmentByName(getChat().decolorize(meta.getDisplayName()));
                            if (enchantment != null) {
                                showEnchantmentWindow((Player) e.getWhoClicked(), e.getClickedInventory().getItem(position(3, 2)), enchantment);
                            }
                        }
                    }
                } else if (slot == position(3, 6)) {
                    if (e.getClickedInventory().getItem(position(3, 2)) != null
                            && e.getClickedInventory().getItem(position(3, 4)) != null
                            && e.getClickedInventory().getItem(position(3, 6)) != null) {
                        if (e.getClickedInventory().getItem(position(3, 2)).getType().equals(e.getClickedInventory().getItem(position(3, 6)).getType())) {
                            ItemStack enchantItem = e.getClickedInventory().getItem(position(3, 4));
                            if (enchantItem.hasItemMeta()) {
                                ItemMeta enchantMeta = enchantItem.getItemMeta();
                                if (enchantMeta.hasDisplayName()) {
                                    Enchantments enchantment = Enchantments.getEnchantmentByName(getChat().decolorize(enchantMeta.getDisplayName()));

                                    if (enchantment != null) {
                                        showCheckoutWindow((Player) e.getWhoClicked(), e.getClickedInventory().getItem(position(3, 6)), enchantment);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else if (e.getInventory().getTitle().equalsIgnoreCase(checkoutTitle)) {
            e.setCancelled(true);

            if (e.getCurrentItem() != null) {
                ItemStack item = e.getCurrentItem();

                if (item.getType().equals(Material.ARROW) && item.hasItemMeta()) {
                    ItemMeta meta = item.getItemMeta();
                    if (meta.hasDisplayName()) {
                        showListWindow((Player) e.getWhoClicked());
                        return;
                    }
                }

                if (item.hasItemMeta()) {
                    ItemMeta meta = item.getItemMeta();

                    if (meta.hasDisplayName()) {
                        String name = getChat().decolorize(meta.getDisplayName());

                        if (name.contains("Pay") && !name.contains("Disabled")) {
                            String cost = name.split(" ")[1];
                            if (cost.contains("$")) {
                                cost = cost.substring(1, cost.length());
                            }

                            Player player = (Player) e.getWhoClicked();
                            double money = -1;
                            int price = -1;

                            try {
                                String action = null;
                                price = Integer.parseInt(cost);

                                switch (item.getType()) {
                                    case DIAMOND:
                                        action = "money";
                                        money = plugin.hook.getVault().getCurrency(player);
                                        break;
                                    case EMERALD:
                                        action = "tokens";
                                        money = plugin.hook.getToken().getCurrency(player);
                                        break;
                                }

                                if (money != -1 && price != -1) {
                                    if (money >= price) {
                                        switch (item.getType()) {
                                            case DIAMOND:
                                                plugin.hook.getVault().takeCurrency(player, price);
                                                break;
                                            case EMERALD:
                                                plugin.hook.getToken().takeCurrency(player, price);
                                                break;
                                        }
                                        // Give the player the item
                                        // player.getInventory().addItem(item);

                                        String playerName = player.getName();
                                        Map<String, Object> itemCache = itemsCache.get(playerName);
                                        String itemCacheStrinified = String.valueOf(itemCache);
                                        int index = -1;

                                        ItemStack[] inventory = player.getInventory().getContents();
                                        for (int i = 0; i < inventory.length; i++) {
                                            if (inventory[i] != null && String.valueOf(inventory[i].serialize()).equals(itemCacheStrinified)) {
                                                index = i;
                                                break;
                                            }
                                        }

                                        if (index != -1) {
                                            ItemStack replacementItem = e.getClickedInventory().getItem(4);

                                            ReplacementData data = getDifferance(ItemStack.deserialize(itemCache), replacementItem);

                                            player.getInventory().setItem(index, replacementItem);
                                            if (data != null) {
                                                getChat().sendMessage(player, messages.getCheckoutMessage(
                                                        player,
                                                        data.enchantment,
                                                        data.getLevel(),
                                                        price,
                                                        ("money".equals(action)) ? "$" : action)
                                                );
                                            }
                                        }

                                        player.closeInventory();
                                    } else {
                                        getChat().sendMessage(player, "&cYou don't have enough " + action + " to buy this enchantment.");
                                    }

                                    player.closeInventory();
                                }
                            } catch (NumberFormatException ex) {

                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e)
    {
        itemsCache.remove(e.getPlayer().getName());
    }

    private ReplacementData getDifferance(ItemStack oldItem, ItemStack newItem)
    {
        int level = -1;
        Enchantments enchantment = null;

        ItemMeta meta = newItem.getItemMeta();
        List<String> lore = meta.getLore();

        if (oldItem.hasItemMeta() && (oldItem.getItemMeta()).hasLore()) {
            ItemMeta oldMeta = oldItem.getItemMeta();
            List<String> oldLore = oldMeta.getLore();

            // New enchantment was added to an already enchanted item stack
            if (lore.size() != oldLore.size()) {
                lore.removeAll(oldLore);

                String[] data = plugin.chat.decolorize(lore.get(0)).split(" ");
                String enchant = "";
                for (int i = 0; i < (data.length - 1); i++) {
                    enchant += data[i] + " ";
                }
                enchantment = Enchantments.getEnchantmentByName(enchant.trim());
                level = stringToEnchantmentLevel(data[data.length - 1].trim());

            } else {
                // Upgraded enchantment
                String diff = null;
                for (int i = 0; i < lore.size(); i++) {
                    if (!lore.get(i).equals(oldLore.get(i))) {
                        diff = lore.get(i);
                        break;
                    }
                }

                if (diff != null) {
                    String[] data = plugin.chat.decolorize(diff).split(" ");
                    String enchant = "";
                    for (int i = 0; i < (data.length - 1); i++) {
                        enchant += data[i] + " ";
                    }
                    enchantment = Enchantments.getEnchantmentByName(enchant.trim());
                    level = stringToEnchantmentLevel(data[data.length - 1].trim());
                }
            }
        } else {
            // New enchantment on an item that doesn't have any enchantments
            String[] data = plugin.chat.decolorize(lore.get(0)).split(" ");
            String enchant = "";
            for (int i = 0; i < (data.length - 1); i++) {
                enchant += data[i] + " ";
            }
            enchantment = Enchantments.getEnchantmentByName(enchant.trim());
            level = stringToEnchantmentLevel(data[data.length - 1].trim());
        }

        return new ReplacementData(enchantment, level);
    }

    private class ReplacementData
    {

        private final Enchantments enchantment;
        private final int level;

        public ReplacementData(Enchantments enchantment, int level)
        {
            this.enchantment = enchantment;
            this.level = level;
        }

        public Enchantments getEnchantment()
        {
            return enchantment;
        }

        public int getLevel()
        {
            return level;
        }
    }
}
