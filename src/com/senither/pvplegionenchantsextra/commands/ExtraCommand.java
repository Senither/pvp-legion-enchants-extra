package com.senither.pvplegionenchantsextra.commands;

import com.senither.pvplegionenchants.Enchantments;
import com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra;
import static com.senither.pvplegionenchantsextra.PvPLegionEnchantsExtra.debug;
import com.senither.pvplegionenchantsextra.chat.ChatFancyText;
import com.senither.pvplegionenchantsextra.utils.ConfigMessages;
import com.senither.pvplegionenchantsextra.utils.Cost;
import com.senither.pvplegionenchantsextra.utils.MaterialContainer;
import com.senither.pvplegionenchantsextra.utils.Permissions;
import java.io.File;
import java.util.logging.Level;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;

public class ExtraCommand extends Command
{

    private final PvPLegionEnchantsExtra plugin;

    public ExtraCommand(PvPLegionEnchantsExtra plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public boolean requireSenderIsPlayer()
    {
        return true;
    }

    @Override
    public boolean runCommand(CommandSender player, String label, String[] args)
    {
        if (args.length != 0 && (args[0].equalsIgnoreCase("author") || args[0].equalsIgnoreCase("creator") || args[0].equalsIgnoreCase("dev"))) {
            plugin.chat.sendMessage(player, "&eLegionEnchants Extra &6was developed by &eAlexis &6/ &eSenither");
            plugin.chat.sendMessage(player, "&6Plugin Version: &e" + plugin.getDescription().getVersion());
            return true;
        }

        if (!player.hasPermission(Permissions.RELOAD)) {
            getChat().missingPermission(player, Permissions.RELOAD);
            return false;
        }

        ChatFancyText msg = getFancyChat();

        if (args.length == 0) {
            msg.sendRawMessage(player.getName(), msg.getJsonMessage("&7[&6+&7]&m--&7[ &6&lLegionEnchant Extras Help Menu &7]&7&m--&7[&6+&7]"));

            // Placeholders
            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchantP &8<&ehelp&8>", "/lenchantp help"),
                    msg.getJsonMessage("&f: Enchant placeholders")
            );

            // List enchantments
            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchantGUI &8<&bplayer&8>", "/lenchantgui player "),
                    msg.getJsonMessage("&f: Opens the GUI for the player")
            );

            // Reload
            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchantExtra &8<&areload&8>", "/lenchantextra reload "),
                    msg.getJsonMessage("&f: Reloads the plugin")
            );
        } else if (args[0].equalsIgnoreCase("reload")) {
            getChat().sendMessage(player, "&8[&aツ&8]&m-&8[ &aReloading Enchants Extra..");

            plugin.reloadConfig();
            plugin.enchantments.reloadConfig();

            if (plugin.getConfig().getInt("version", -1) != PvPLegionEnchantsExtra.configVersion) {
                plugin.chat.getLogger().info("[Error] Config version mismatch!");
                plugin.chat.getLogger().info("[Error] Resetting config back to default.");
                File tempConfig = new File(plugin.getDataFolder(), "config.yml");
                if (tempConfig.exists()) {
                    tempConfig.delete();
                }
                plugin.saveDefaultConfig();
                plugin.reloadConfig();
            }

            PvPLegionEnchantsExtra.debug = plugin.getConfig().getBoolean("debug-messages-enabled", false);

            int update = plugin.getConfig().getInt("update-time", 5);
            if (update < 1) {
                update = 1;
            }
            plugin.setUpdate(update);

            // Reloads GUI messages
            ConfigMessages.reload();

            plugin.cost.clear();
            plugin.materials.clear();
            for (Enchantments enchantment : Enchantments.values()) {
                String name = enchantment.getName().replace(" ", "_").toLowerCase();
                Cost costContainer = new Cost();

                if (!plugin.enchantments.getConfig().contains(name)) {
                    plugin.enchantments.getConfig().set(name + ".material", Material.STONE.toString() + ":0");
                    for (int i = 1; i <= enchantment.getLevel(); i++) {
                        plugin.enchantments.getConfig().set(name + ".cost.upgrade-" + i + ".money", 9001);
                        plugin.enchantments.getConfig().set(name + ".cost.upgrade-" + i + ".token", 9001);
                        costContainer.setMoneyCost(i, -1).setTokenCost(i, -1);
                    }
                    plugin.materials.put(enchantment, new MaterialContainer(Material.STONE, (short) 0));
                } else {
                    String mat = plugin.enchantments.getConfig().getString(name + ".material", "l:l");
                    String[] d = mat.split(":");
                    Material material = Material.getMaterial(d[0]);
                    if (material != null && material != Material.AIR) {
                        plugin.materials.put(enchantment, new MaterialContainer(material, Short.parseShort(d[1])));
                    } else {
                        if (debug) {
                            plugin.chat.getLogger().log(Level.INFO, "Invalid material given for enchantment \"{0}\"! Resetting back to stone.", enchantment.getName());
                            plugin.enchantments.getConfig().set(name + ".material", Material.STONE.toString() + ":0");
                        }
                        plugin.materials.put(enchantment, new MaterialContainer(Material.STONE, (short) 0));
                    }

                    for (int i = 1; i <= enchantment.getLevel(); i++) {
                        int money = plugin.enchantments.getConfig().getInt(name + ".cost.upgrade-" + i + ".money", -1);
                        int token = plugin.enchantments.getConfig().getInt(name + ".cost.upgrade-" + i + ".token", -1);
                        costContainer.setMoneyCost(i, money).setTokenCost(i, token);
                    }
                }
                plugin.cost.put(enchantment, costContainer);
            }
            plugin.enchantments.saveConfig();

            getChat().sendMessage(player, "&8[&aツ&8]&m-&8[ &aLegion Extra v" + plugin.getDescription().getVersion() + " has been reloaded successfully!");
        } else {
            String command = "/LEncE ";
            for (String arg : args) {
                command += arg + " ";
            }
            invalidCommand(player, command.trim());
        }
        return true;
    }
}
